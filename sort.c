// Code written by:
// YEO, YIH TANG
// UTORID: yeoyih
// Student Number: 999295132
// CSC192 Assignment 3

#include <stdio.h>
#include <stdlib.h>
#define MAXCHARS 100

int find_left_child(int index_position_of_parent);
int find_right_child(int index_position_of_parent);
int find_parent(int child_index_position);
void satisfy_condition_for_node(int a[], int node_of_index, int length_of_array);
void satisfy_condition_for_all_nodes(int a[], int number_of_non_leaf_nodes, int number_of_nodes);
void perform_sorting(int a[], int size_of_array);

int binary_summation(int index);                    // Extra function: sum of the power of 2 in this way: 2^0 + 2^1 + 2^2 + ... + 2^n
int power_of_two(int exponent);                     // Extra function: using recursion to determine the power of 2 rather than using math.h
int determine_tree_level(int index);                // Extra function: calculate which "level" the node is currently at
void swap_values(int *ptr1, int *ptr2);             // Extra function: using pointers to swap values between two variables

int main()
{
    FILE *inputFile;
    char fileName[MAXCHARS];                        // variable to store user-input file name
    char currentString[MAXCHARS];                   // variable to store the array of char current line when the fgets is reading a specific line
    int arrayDataTree[MAXCHARS];                    // the array that will act as binary tree
    int numberOfNodes;                              // variable used to store the length of the array, which is also the number of nodes of the binary tree

    // request user to enter the file name that has the binary tree
    printf("Please enter the name of the text file to read in binary tree: ");
    scanf("%s", fileName);

    // attempt to open the file to be read "r"
    inputFile = fopen(fileName, "r");

    // Error Detection: if the file is empty (does not exist), terminate the program immediately, if not, proceed to analyzing the inputFile
    if (inputFile == NULL){
        printf("Sorry, file cannot be opened or is not present.\n");
        return 0;
    }
    else{

        // part 1: converting file into array which will be used as data tree
        // first line of the file: the number of nodes in the file. Read it using fgets and convert it into integers and store into the variable numberOfNodes
        fgets(currentString, MAXCHARS, inputFile);
        numberOfNodes = atoi(currentString);

        // if the number of nodes as specified in the input file is a negative number, it will freeze the program and cause error
        // therefore, we have to check for it to ensure that the number is 0 or larger than 0
        if (numberOfNodes < 0){
            printf("Error: the number of nodes should be a positive number.\n");
            return 0;
        }

        // second line should be an empty line, try to read using fgets and determine if it's really a empty newline
        fgets(currentString, MAXCHARS, inputFile);
        // Error Detection: if the line is not a new line, then show error message and terminate the program
        if (currentString[0] != '\n'){
            printf("File is not properly formatted to be read.\n");
            return 0;
        }

        // third line and beyond are the values for the data trees
        int i;
        // initialize a loop and run it for n times, where n is the number of nodes as specified in the first line of the file
        // PART 1: STORE DATA TREE INTO ARRAY
        for (i = 0; i < numberOfNodes; i++){
            fgets(currentString, MAXCHARS, inputFile);
            // convert the line into integer and store into an array
            arrayDataTree[i] = atoi(currentString);
        }

        // print the original file before performing satisfy_condition
        printf("\nThe binary tree read directly from text file is as: \n");
        for (i = 0; i < numberOfNodes; i++){
            printf("%d at index=%d\n", arrayDataTree[i], i);
        }

        // Algorithm for Part 4: Calculate for number of non leaf
        int numberOfNonLeaf = 0;
        numberOfNonLeaf = (numberOfNodes)/2;
        // note that no matter the numberOfNodes is odd or even, when we divide it by 2, we will always get the number of non-leaves node

        // PART2: go through all the nodes and to make all of them satisfy condition
        for (i = 0; i < numberOfNodes; i++){
            satisfy_condition_for_node(arrayDataTree, i, numberOfNodes);
        }

        // PART3: call the function that will repeat satisfy_condition_for_node to make sure all nodes really satisfy condition
        satisfy_condition_for_all_nodes(arrayDataTree, numberOfNonLeaf, numberOfNodes);

        // After performing satisfy_condition, print the processed array
        printf("\nThe binary tree after satisfy conditions for all nodes: \n");
        for (i = 0; i < numberOfNodes; i++){
            printf("%d at index=%d\n", arrayDataTree[i], i);
        }

        // PART4: call the function that will perform sorting of the nodes from lowest to highes value
        perform_sorting(arrayDataTree, numberOfNodes);

        // print out the sorted array
        printf("\nThe binary tree after sorting: \n");
        for (i = 0; i < numberOfNodes; i++){
            printf("%d at index=%d\n", arrayDataTree[i], i);
        }


    }

    return 0;
}

void perform_sorting(int a[], int size_of_array){
    int i;

    // BASE STATEMENT for recursion: because we are removing each node from the data tree, the size of the data tree (which is also the array),
    // virtually, will decrease by 1. If it reaches 0, no sorting has to be done, as all data tree members are no longer hold in a tree structure.
    if (size_of_array > 0){

        // create a temporary variable that stores the value of the first index, which is the largest value in the data tree
        // this is to make sure that the largest value is still stored while we perform the swapping that will happen next
        int temporary_int = a[0];

        // then, move all the elements up, for such a[1] replace a[0], a[2] re[laces a[1], a[3] replaces a[2] and such
        for (i = 0; i < size_of_array-1; i++){
            a[i] = a[i+1];
        }

        // Virtually remove the largest value from the array by putting the value at the last index of the array
        a[size_of_array-1] = temporary_int;

        // the array is smaller by one;

        // by removing the largest value from the array, we messed up the conditions for the data tree (excluding the last element of array that
        // is considered separated from the data tree). Therefore, we have to call the satisfy_condition_for_all_nodes to make the data tree back in
        // good shape. Note that size_of_array is reduced by 1 because the last element of the array is no longer part of the data tree.
        int num_of_non_leaf = (size_of_array-1)/2;
        satisfy_condition_for_all_nodes(a, num_of_non_leaf, size_of_array-1);

        // then perform recursion again
        perform_sorting(a, size_of_array-1);
    }

}

void satisfy_condition_for_all_nodes(int a[], int number_of_non_leaf_nodes, int number_of_nodes){
    int i, j;

    // tree level is to determine which level (counting 1 from the top of the tree) so that we know how many times the sorting should be done,
    // in order to push up a value from the lowest position to the highest position, in case the lowest level node has the biggest value among
    // all other nodes. This is determined by an extra function.
    int tree_level = determine_tree_level(number_of_non_leaf_nodes);

    // OUTER LOOP: repeat n times according to the number of level.
    for (j = 0; j <= tree_level; j++){

        // INNER LOOP: check each node of the data tree to make sure all of them satisfy the conditions mentioned...
        for (i = 0; i < number_of_non_leaf_nodes; i++){

            // ... and this is done by calling the individual function satisfy_condition_for_node and specifying the index of the specific node
            satisfy_condition_for_node(a, i, number_of_nodes);
        }
    }
}

// this function uses pointer to "pass variable by reference" to swap two values between two variables
void swap_values(int *ptr1, int *ptr2){
    int temp_variable;

    // derefencing the pointer and store its value into a temporary variable
    temp_variable = *ptr1;
    // deferencing both pointers to make them equal
    *ptr1 = *ptr2;
    // deferencing ptr2 and store the value of temp_variable
    *ptr2 = temp_variable;
}

/*
this function takes the parent node to compare with its children (left and right, if they exist) to see if the parents are larger
than both children.  If the parent is smaller than one of its children, find the larger one among its children, and then swap
the values between the parent and the larger children.
*/
void satisfy_condition_for_node(int a[], int node_of_index, int length_of_array)
{
    // using two variables to store the index of the children (left and right) by calling the respective functions
    int leftIndex = find_left_child(node_of_index), rightIndex = find_right_child(node_of_index);
    // create another two variables that will store the values hold by the index of the children
    int leftChild = a[leftIndex], rightChild = a[rightIndex];

    // this is the BASE STATEMENT FOR RECURSION: if the parent node is larger than the number of nodes, don't do anything.
    if (node_of_index < length_of_array){

        // this IF statement is to check if the LEFT CHILD EXIST. If the left child exists, it should be smaller than the number of nodes of the data tree
        if (leftIndex < length_of_array){

            // this IF statement is to check if the RIGHT CHILD EXIST. If the right child exists, it should be smaller than the number of nodes of the data tree
            // NOTE THAT after this line, the code will know that both left and right children exist so that it can compare the values between them
            if (rightIndex < length_of_array){

                // check IF the parent is smaller than the children
                if (a[node_of_index] < rightChild || a[node_of_index] < leftChild){

                    // if the parent is indeed smaller, check to see to compare either the left or right child holds a larger value
                    if (rightChild > leftChild){

                        // if the right child is bigger, call the function to swap values between right child and parent
                        swap_values(&a[node_of_index], &a[rightIndex]);

                        // RECURSION: because after the swipping process is done, the child (which also might be a parent node) will not satisfy the
                        // next tree it is involved in. Therefore, repeat this function but now the right child is a parent.
                        satisfy_condition_for_node(a, rightIndex, length_of_array);
                    }
                    else{

                        // if the left child is bigger, call the function to swap values between left child and parent
                        swap_values(&a[node_of_index], &a[leftIndex]);

                        // RECURSION: because after the swipping process is done, the child (which also might be a parent node) will not satisfy the
                        // next tree it is involved in. Therefore, repeat this function but now the left child is a parent.
                        satisfy_condition_for_node(a, leftIndex, length_of_array);
                    }
                }
            }

            // let's say it has only one child on the left (obviously it can't have a right child without a left child), then there is no need to compare
            // the left and right child when deciding which one to be swapped with the parent, if the parent is smaller than the child.
            else{

                // check if the ONLY child (which is the left one) is larger than parent
                if (a[node_of_index] < leftChild){

                    // if the left child is bigger, call the function to swap values between left child and parent
                    swap_values(&a[node_of_index], &a[leftIndex]);

                    // RECURSION: because after the swipping process is done, the child (which also might be a parent node) will not satisfy the
                    // next tree it is involved in. Therefore, repeat this function but now the left child is a parent.
                    satisfy_condition_for_node(a, leftIndex, length_of_array);
                }
            }
        }
    }


}

int find_left_child(int index_position_of_parent)
{
    // algorithm to find the left child index
    return index_position_of_parent*2+1;
}

int find_right_child(int index_position_of_parent)
{
    // algorithm to find the right child index
    return index_position_of_parent*2+2;
}

int find_parent(int child_index_position)
{
    // algorithm to find the parent index
    // note that if the parent is the topmost parent, return a 0 to avoid negative index that might crash the program
    if (child_index_position==0)
        return 0;
    else
        return (child_index_position-1)/2;
}

int determine_tree_level(int index)
{
    // if the index is 0, it must be the topmost level, so return 0
    if (index == 0){
        return 0;
    }
    else{
        // create a variable i (to be incremented) to store the level
        int i = 0;

        // there is a pattern for the starting index and the ending index of a certain level (which is determined by the function binary_summation)
        // if index falls between the range, yes, it is the level. If not, keep incrementing i to check with the next level.
        // for example, level 1: start: 1, end: 2
        // for example, level 2: start: 3, end: 6
        // for example, level 3: start: 7, end: 14
        // binary_summation(2) gives a value of 3, and binary_summation(2+1) returns 7. Since level 2 ends at 6,
        // which is 1 lesser than binary_summation(2_1), we have to add -1 behind it
        while(!(index >= binary_summation(i) && index <= (binary_summation(i+1)-1))){
            i++; // increment i to check if i falls into the next level
        }
        //when it's done, return the value of i
        return i;
    }
}

int binary_summation(int index)
{
    // sumOfBinary taking in the total power of 2, i is the counter
    int sumOfBinary = 0, i=0;
    for (i = 0; i < index; i++){
        // using a loop, call the function power_of_two to find the value of 2^i, where i keeps incrementing from 0 to the value specified by "index"
        sumOfBinary += power_of_two(i);
    }
    // when it's done, return the sum of the binary
    return sumOfBinary;
}

int power_of_two(int exponent)
{
    // BASE statement, if exponent is 0, return 1
    if (exponent == 0)
        return 1;
    // RECURSIVE statement, takes two and times with the power_of_two(exponent-1)
    else
        return 2*power_of_two(exponent-1);
}
